//<head>//////////////////////////////////////////////////////////////
//
//	
//	Lexed version 4
//	Copyright (C) 1999 2000 2001 2002 2003, 2006.
//	Universit� Paris 7
//	INRIA Rocquencourt
//
//	Conception & programmation :
//	Lionel Cl�ment
//	e-mail : lionel.clement@linguist.jussieu.fr
//
//
////////////////////////////////////////////////////////////////</head>////

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "liblexed.h"

extern char error[LEXEDMAXSTRING];
extern int memoire;
extern int indexation;
extern char sep_pref[LEXEDMAXSTRING];
extern char sep_or[LEXEDMAXSTRING];
extern char sep_suff[LEXEDMAXSTRING];
extern char sep_uw[LEXEDMAXSTRING];
extern char delimiter;
extern TYPEPTR initial;
  
////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////
void usage(int value) {
  fprintf(stderr,
    "usage: lexed [global-option] <build|consult|list> [mode-options] <filename>\n"
    "Global options:\n"
    "-h|--help                              print this\n"
    "-v|--version                           print version\n"
    "-p <name>                              prefix name\n"
    "-d <directory>                         directory\n"
    "Options for build mode:\n"
    "-i                                     indexation mode\n"
    "-t <char>                              delimiter character\n"
    "Options for consult mode:\n"
    "-f <pref> <or> <suff> <uw>             format information\n"
    "-P <port>                              port number (daemon)\n"
    "-F                                     look for information in a file rather than in memory\n"
    "-e <number>                            number of characters to erase\n"
    "-s <number>                            number of characters to subtitute\n"
    "-a <number>                            number of characters to add\n"
    "-w <number>                            number of characters to swap\n"
    "-b <number>                            number of double characters to erase\n"
    "-B <number>                            number of double characters to add\n"
    "-S <number>                            number of characters to translate\n"
    "-r <chars to seach> <chars to replace> add a charactere translation schema\n"
    "-W <number>                            the maximum weight\n"
  );
  exit(value);
}

void version(int value) {
  fprintf(stderr, "%s version %s\n", PACKAGE, VERSION);
  exit(value);
}

int build(int argn, char **argv, int pos, char* directory, char* prefix) {

  FILE *input;
  char input_file[LEXEDMAXSTRING];
  int i;
  struct stat statbuf;
  long input_size;
  strcpy(input_file,"");
  indexation=0;
  
  // mode specific options
  for (i = pos; argv[i]; i++) {
    if (argv[i][0] == '-') {
      if (!strcmp(argv[i]+1, "i")) {
	indexation = 1;
	strcpy(sep_uw, "0");
      } else if (!strcmp(argv[i]+1, "t")) {
	if (i+1 >= argn || argv[i+1][0] == '-') {
	  usage(EXIT_FAILURE);
	} else {
	  delimiter = argv[+i][0];
	}
      }
    } else {
      strncpy(input_file, argv[i], LEXEDMAXSTRING);
    }
  }

  if (input_file && strcmp(input_file, "")) {
    fprintf(stderr, "*** Loading lexicon from file '%s'\n", input_file);
    input = fopen (input_file, "r");
    if (!input) {
      snprintf(error, LEXEDMAXSTRING, "Unable to open file %s for reading", input_file);

      perror(error);
      return -1;
    }
    stat(input_file, &statbuf);
    input_size = statbuf.st_size;
  } else {
    fprintf(stderr, "*** Loading lexicon from stdin\n");
    input = stdin;
    input_size = 0;
  }

  load_from_input(input, input_size);
  save_to_files(directory, prefix);

  if (input_file) {
    fclose (input);
  }

  return 0;
}

int consult(int argn, char **argv, int pos, char* directory, char* prefix) {

  FILE *input;
  char input_file[LEXEDMAXSTRING];
  char string[LEXEDMAXSTRING];
  int i;
  char *buf;
  TYPEPTR info;
  Server *myserver;
  int port = 0;
  int server = 0;
  int nb_erase       = 0;
  int nb_substitute  = 0;
  int nb_add         = 0;
  int nb_swap        = 0;
  int nb_erase_double = 0;
  int nb_add_double   = 0;
  int nb_replace     = 0;
  struct FindReplaceStructure *replace;
  int replace_index;
  int replace_size;
  TYPEPTR max_weight=(~0UL);

  strcpy(input_file,"");
  
  // mode specific options
  for (i = pos; i < argn && argv[i]; i++) {
    if (argv[i][0] == '-') {
	if (!strcmp(argv[i]+1, "f")) {	
	  if (i+4 >= argn || argv[i+1][0] == '-' || argv[i+2][0] == '-' || argv[i+3][0] == '-' || argv[i+4][0] == '-') {
	    usage(EXIT_FAILURE);
	  } else {
	    strncpy(sep_pref, argv[++i], LEXEDMAXSTRING);
	    strncpy(sep_or, argv[++i], LEXEDMAXSTRING);
	    strncpy(sep_suff, argv[++i], LEXEDMAXSTRING);
	    strncpy(sep_uw, argv[++i], LEXEDMAXSTRING);
	  }
	} else if (!strcmp(argv[i]+1, "P")) {
	  if ((i+1 >= argn) || (argv[i+1][0]=='-')) {
	    usage(EXIT_FAILURE);
	  } else {
	    port = atoi(argv[++i]);
	  }
	} else if (!strcmp(argv[i]+1, "F")) {
	  memoire = 0;
	} else if (!strcmp(argv[i]+1, "e")) {
	  if (i+1 >= argn || argv[i+1][0] == '-' || atoi(argv[i+1]) == 0) {
	    usage(EXIT_FAILURE);
	  } else {
	    nb_erase=atoi(argv[++i]);
	  }
	} else if (!strcmp(argv[i]+1, "s")) {
	  if ((i+1 >= argn) || (argv[i+1][0] == '-') || (atoi(argv[i+1]) == 0)) {
	    usage(EXIT_FAILURE);
	  } else {
	    nb_substitute=atoi(argv[++i]);
	  }
	} else if (!strcmp(argv[i]+1, "S")) {
	  if (i+1 >= argn || argv[i+1][0] == '-' || atoi(argv[i+1]) == 0) {
	    usage(EXIT_FAILURE);
	  } else {
	    nb_replace=atoi(argv[++i]);
	  }
	} else if (!strcmp(argv[i]+1, "a")) {
	  if (i+1 >= argn || argv[i+1][0] == '-' || atoi(argv[i+1]) == 0) {
	    usage(EXIT_FAILURE);
	  } else {
	    nb_add=atoi(argv[++i]);
	  }
	} else if (!strcmp(argv[i]+1, "w")) {
	  if (i+1 >= argn || argv[i+1][0] == '-' || atoi(argv[i+1]) ==0 ) {
	    usage(EXIT_FAILURE);
	  } else {
	    nb_swap=atoi(argv[++i]);
	  }
	} else if (!strcmp(argv[i]+1, "b")) {
	  if (i+1 >= argn || argv[i+1][0] == '-' || atoi(argv[i+1]) == 0) {
	    usage(EXIT_FAILURE);
	  } else {
	    nb_erase_double=atoi(argv[++i]);
	  }
	} else if (!strcmp(argv[i]+1, "B")) {
	  if (i+1 >= argn || argv[i+1][0] == '-' || atoi(argv[i+1]) == 0) {
	    usage(EXIT_FAILURE);
	  } else {
	    nb_add_double=atoi(argv[++i]);
	  }
	} else if (!strcmp(argv[i]+1, "r")) {
	  if (i+2 >= argn || argv[i+1][0] == '-' || argv[i+2][0] == '-') {
	    usage(EXIT_FAILURE);
	  } else {
	    if (replace == NULL) {
	      replace_size=LEXEDMAXREPLACE;
	      replace = (struct FindReplaceStructure *)calloc(sizeof(struct FindReplaceStructure), LEXEDMAXREPLACE);
	      replace_index=0;
	      replace[replace_index].find = NULL;
	      replace[replace_index].replace = NULL;
	    }
	    if (replace_index == replace_size) {
	      replace_size *= 2;
	      replace = (struct FindReplaceStructure *)realloc(replace, sizeof(struct FindReplaceStructure) * replace_size);
	      replace[replace_index].find = NULL;
	      replace[replace_index].replace = NULL;
	    }
	    replace[replace_index].find=(char *)calloc(strlen(argv[++i])+1, sizeof(char));
	    strncpy(replace[replace_index].find, argv[i], LEXEDMAXSTRING);
	    replace[replace_index].replace=(char *)calloc(strlen(argv[++i])+1, sizeof(char));
	    strncpy(replace[replace_index].replace, argv[i], LEXEDMAXSTRING);
	    replace_index++;
	  }
	} else if (!strcmp(argv[i]+1, "W")) {
	  if (i+1 >= argn || argv[i+1][0] == '-' || atoi(argv[i+1]) == 0) {
	    usage(EXIT_FAILURE);
	  } else {
	    max_weight=(TYPEPTR)atoi(argv[++i]);
	  }
	}
      } else {
      strncpy(input_file, argv[i], LEXEDMAXSTRING);
    }
  }

  if (load_from_files(directory, prefix) < 0) {
    perror("unable to initialise lexicon");
    return -1;
  }

  if (server == 1) {
    if (port < 1 || port > 65535) {
      snprintf(error, LEXEDMAXSTRING, "Invalid port number %d", port);

      perror(error);
      finish();
      return -1;
    } else {
      myserver = new Server(port);
    }

    fprintf(stderr, "*** Ready for connection with a client on port %d\n", port);
#ifdef DAEMON
    daemon(0,0);
#endif
    myserver->start();
    while (myserver->get_data()!=-1) {
      buf=myserver->get_buffer();
      if (buf[0]==-1)
	break;
      if (nb_erase || nb_substitute || nb_add || nb_swap || nb_erase_double || nb_add_double || nb_replace){
	info = search_static(initial, string);
	if (info!=(TYPEPTR)~0UL) {
	  print_results(info, 1, myserver, 1);
	} else {
	  int out=0;
	  int result_size=0;
	  int result_index=0;
	  TYPEPTR *result=NULL;
	  search(initial, buf, nb_erase, nb_substitute, nb_add, nb_swap, nb_erase_double, nb_add_double, nb_replace, replace, 0, &result, &result_size, &result_index, 1);
	  for (TYPEPTR *r=result; r!=NULL && *r!=(TYPEPTR)~0UL; r++, r++){
	    if ((max_weight == (~0UL)) ||
		((*(r+1)) <= max_weight)) {
	      if (out) {
		myserver->put_data(sep_or);
	      }
	      out = 1;
	      print_results ((*r), 1, myserver, 0);
	    }
	  }
	  if (!out) {
	    myserver->put_data(sep_uw);
	  }
	  myserver->put_data(sep_suff);
	}
      } else {
	info = search_static(initial, buf);
	print_results(info, 1, myserver, 1);
      }
    }
    myserver->stop();
  } else {
    if (input_file && strcmp(input_file, "")) {
      fprintf(stderr, "*** Seaching %s\n", input_file);
      input = fopen (input_file, "r");
      if (!input) {
	snprintf(error, LEXEDMAXSTRING, "Unable to open file %s for reading", input_file);
	perror(error);
	return -1;
      }
    } else {
      fprintf(stderr, "*** Searching from stdin\n");
      input = stdin;
    }
    while (fgets (string, LEXEDMAXSTRING, input)) {
      //
      string[strlen(string)-1]=0;
      if (nb_erase || nb_substitute || nb_add || nb_swap || nb_erase_double || nb_add_double || nb_replace){
	int out=0;
	int result_size=0;
	int result_index=0;
	TYPEPTR *result=NULL;
	search(initial, string, nb_erase, nb_substitute, nb_add, nb_swap, nb_erase_double, nb_add_double, nb_replace, replace, 0, &result, &result_size, &result_index, 1);
	for (TYPEPTR *R=result; R!=NULL && (*R)!=(TYPEPTR)~0UL; R++, R++){
	  if ((max_weight == (~0UL))||
	      ((*(R+1)) <= max_weight)){
	    if (out) {
	      fputs(sep_or, stdout);
	    }
	    out = 1;
	    print_results ((*R), 0, NULL, 0);
	  }
	}
	if (!out) {
	  fputs(sep_uw, stdout);
	}
	fputs(sep_suff, stdout);
      } else {
	info = search_static(initial, string);
	print_results(info, 0, myserver, 1);
      }
      fflush(stdout);
    }
  }

  if (input_file) {
    fclose(input);
  }

  finish();

  return 0;
}

int list(int argn, char **argv, int pos, char* directory, char* prefix) {

  if (load_from_files(directory, prefix) < 0) {
    perror("unable to initialise lexicon");
    return -1;
  }

  list_result result = list();
  for (int i = 0; i < result.items_size; i++) {
    printf("%s\t%s\n", result.words + result.items[i].word_offset, result.items[i].info);
  }

  free_list_result(&result);

  finish();

  return 0;
}

////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////
int main(int argn, char **argv) {
  char prefix[LEXEDMAXSTRING]    = "lexicon";
  char directory[LEXEDMAXSTRING] = ".";
  int result;
  int i;

  if (argn > 1) {
    // generic options
    for (i = 1; argv[i]; i++) {
      if (argv[i][0] == '-') {
	if (!strcmp(argv[i]+1, "p")) {
	  if ((i+1 >= argn) || argv[i+1][0] == '-') {
	    usage(EXIT_FAILURE);
	  } else {
	    strncpy(prefix, argv[++i], LEXEDMAXSTRING);
	  }
	} else if (!strcmp(argv[i]+1, "d")) {
	  if ((i+1 >= argn) || argv[i+1][0] == '-') {
	    usage(EXIT_FAILURE);
	  } else {
	    strncpy(directory, argv[++i], LEXEDMAXSTRING);
	  }
	} else if (!strcmp(argv[i]+1, "v") || !strcmp(argv[i]+1, "-version")) {
	  version(EXIT_SUCCESS);
	} else if (!strcmp(argv[i]+1, "h") || !strcmp(argv[i]+1, "-help")) {
	  usage(EXIT_SUCCESS);
	} else {
	  usage(EXIT_FAILURE);
	}
      } else {
	if (!strcmp(argv[i], "build")) {
	  result = build(argn, argv, i+1, directory, prefix);
          break;
	} else if (!strcmp(argv[i], "consult")) {
	  result = consult(argn, argv, i+1, directory, prefix);
          break;
	} else if (!strcmp(argv[i], "list")) {
	  result = list(argn, argv, i+1, directory, prefix);
          break;
	} else {
	  usage(EXIT_FAILURE);
	}
      }
    }
  } else {
    usage(EXIT_FAILURE);
  }

  
  
  
  if (result) {
    exit(EXIT_FAILURE);
  } else {
    exit(EXIT_SUCCESS);
  }
}
