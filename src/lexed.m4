dnl Autoconf macros for lexed
dnl $Id$
dnl inspired from gpme package


dnl AM_PATH_LEXED([MINIMUM-VERSION,
dnl               [ACTION-IF-FOUND [, ACTION-IF-NOT-FOUND ]]])
dnl Test for liblexed and define LEXED_CFLAGS and LEXED_LIBS
dnl
AC_DEFUN([AM_PATH_LEXED],
[ AC_PREREQ(2.57)dnl
  AC_ARG_WITH(lexed-prefix,
            AC_HELP_STRING([--with-lexed-prefix=PFX],
                           [prefix where LEXED is installed (optional)]),
     lexed_config_prefix="$withval", lexed_config_prefix="")
  if test x$lexed_config_prefix != x ; then
     lexed_config_args="$lexed_config_args --prefix=$lexed_config_prefix"
     if test x${LEXED_CONFIG+set} != xset ; then
        LEXED_CONFIG=$lexed_config_prefix/bin/lexed-config
     fi
  fi

  AC_PATH_PROG(LEXED_CONFIG, lexed-config, no)
  min_lexed_version=ifelse([$1], ,0.3.9,$1)
  AC_MSG_CHECKING(for LEXED - version >= $min_lexed_version)
  ok=no
  if test "$LEXED_CONFIG" != "no" ; then
    req_major=`echo $min_lexed_version | \
               sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\.\([[0-9]]*\)/\1/'`
    req_minor=`echo $min_lexed_version | \
               sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\.\([[0-9]]*\)/\2/'`
    req_micro=`echo $min_lexed_version | \
               sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\.\([[0-9]]*\)/\3/'`
    lexed_config_version=`$LEXED_CONFIG $lexed_config_args --version`
    major=`echo $lexed_config_version | \
               sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\.\([[0-9]]*\).*/\1/'`
    minor=`echo $lexed_config_version | \
               sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\.\([[0-9]]*\).*/\2/'`
    micro=`echo $lexed_config_version | \
               sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\.\([[0-9]]*\).*/\3/'`
    if test "$major" -gt "$req_major"; then
        ok=yes
    else 
        if test "$major" -eq "$req_major"; then
            if test "$minor" -gt "$req_minor"; then
               ok=yes
            else
               if test "$minor" -eq "$req_minor"; then
                   if test "$micro" -ge "$req_micro"; then
                     ok=yes
                   fi
               fi
            fi
        fi
    fi
  fi
  if test $ok = yes; then
    LEXED_CFLAGS=`$LEXED_CONFIG $lexed_config_args --cflags`
    LEXED_LIBS=`$LEXED_CONFIG $lexed_config_args --libs`
    AC_MSG_RESULT(yes)
    ifelse([$2], , :, [$2])
  else
    LEXED_CFLAGS=""
    LEXED_LIBS=""
    AC_MSG_RESULT(no)
    ifelse([$3], , :, [$3])
  fi
  AC_SUBST(LEXED_CFLAGS)
  AC_SUBST(LEXED_LIBS)
])

