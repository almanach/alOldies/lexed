//<head>//////////////////////////////////////////////////////////////
//
//	
//	Lexed version 3.1
//	Copyright (C) 1999 2000 2001 2002 2003.
//	Universit� Paris 7
//	INRIA Rocquencourt
//
//	Conception & programmation :
//	Lionel Cl�ment
//	e-mail : lionel.clement@linguist.jussieu.fr
//
//	vendredi 18 janvier 2002, 13h41
//
//
////////////////////////////////////////////////////////////////</head>////

#include "lexedtree.h"

Tree::Tree(Tree *son, Tree *brother, Info *info, char car) {
  this->adress = 0;
  this->son = son;
  this->brother = brother;
  this->info = info;
  this->car = car;
}

////////////////////////////////////////////////////////////
// Calcule les offsets du tableau FSA
////////////////////////////////////////////////////////////
void Tree::set_index_fsa(TYPEPTR &index)
{
  Tree *lexique_sy;
  for (lexique_sy=this;lexique_sy!=NULL;lexique_sy=lexique_sy->get_brother()){
    lexique_sy->adress = index++;
    if (lexique_sy->get_son())
      lexique_sy->get_son()->set_index_fsa(index);
  }
}

////////////////////////////////////////////////////////////
// �crit les enregistrements du FSA
// les offsets sont calcul�s sur 32 (vs 16) bits si long==1
////////////////////////////////////////////////////////////
void Tree::print_fsa(Tree *lexique_init, TYPEPTR *initial, FILE *out)
{
  FSA elt;
  Tree *lexique_sy;
  if (this==lexique_init){
    *initial=this->adress;
  }
  for (lexique_sy=this;lexique_sy;lexique_sy=lexique_sy->get_brother()){
    elt.son=(lexique_sy->get_son()==NULL)?(TYPEPTR)~0UL:lexique_sy->get_son()->get_adress();
    elt.brother=(lexique_sy->get_brother()==NULL)?(TYPEPTR)~0UL:lexique_sy->get_brother()->get_adress();
    elt.info=(lexique_sy->get_info()==NULL)?(TYPEPTR)~0UL:lexique_sy->get_info()->get_adress();
    elt.car=lexique_sy->get_car();
    fwrite(&elt, sizeof(elt), 1, out);
#ifdef DIFF
    fprintf(stdout, " so:%lX br:%lX in:%lX <%c>\n", elt.son, elt.brother, elt.info, elt.car);
#endif //DIFF
    lexique_sy->get_son()->print_fsa(lexique_init, initial, out);
  }
}

////////////////////////////////////////////////////////////
// Calcule les offsets du tableau info
////////////////////////////////////////////////////////////
void Tree::set_index_info(TYPEPTR &index)
{
  Info *info_sy;
  Tree *lexique_sy;
  for (lexique_sy=this;lexique_sy!=NULL;lexique_sy=lexique_sy->get_brother())
    lexique_sy->get_son()->set_index_info(index);
  for (lexique_sy=this;lexique_sy!=NULL;lexique_sy=lexique_sy->get_brother()){
    for (info_sy=lexique_sy->get_info();info_sy!=NULL;info_sy=info_sy->get_next()){
      info_sy->set_adress(index++);
    }
  }
}

////////////////////////////////////////////////////////////
// �crit les enregistrement du tableau d'infos
// les offsets sont calcul�s sur 32 (vs 16) bits si long==1
////////////////////////////////////////////////////////////
void Tree::print_info(FILE *out){
  Info *info_sy;
  Tree *lexique_sy;
  InfoBuff elt;
  for (lexique_sy=this;lexique_sy!=NULL;lexique_sy=lexique_sy->get_brother())
    lexique_sy->get_son()->print_info(out);
  for (lexique_sy=this;lexique_sy!=NULL;lexique_sy=lexique_sy->get_brother()){
    for (info_sy=lexique_sy->get_info();info_sy!=NULL;info_sy=info_sy->get_next()){
      elt.next=(info_sy->get_next()!=NULL)?info_sy->get_next()->get_adress():(TYPEPTR)(~(0UL));
      elt.offset=info_sy->get_offset();
      fwrite(&elt, sizeof(elt), 1, out);
#ifdef DIFF
      fprintf(stdout, " su:%lX of:%lX\n", elt.next, elt.offset);
#endif //DIFF
    }
  }
}

////////////////////////////////////////////////////////////
// ajoute un mot dans l'arbre � lettres
////////////////////////////////////////////////////////////
void Tree::add(char *string, TYPEPTR offset){
  Info *info_sy;
  if (!this->car){
    this->car = string[0];
    if (!string[1]) {
      if (this->info) {
	for(info_sy=this->info;info_sy->get_next()!=NULL;info_sy=info_sy->get_next());
	info_sy->set_next(new Info(NULL, offset));
      } else {
	this->info = new Info(NULL, offset);
      }
    } else {
      if (!this->son) {
	this->son = new Tree(NULL, NULL, NULL, string[1]);
      }
      this->son->add(string + 1, offset);
    }
  } else {
    if (this->car == string[0]) {
      if (!string[1]){
	if (this->info){
	  for(info_sy=this->info;info_sy->get_next()!=NULL;info_sy=info_sy->get_next());
	  info_sy->set_next(new Info(NULL, offset));
	} else {
	  this->info = new Info(NULL, offset);
	}
      } else {
	if (!this->son) {
	  this->son = new Tree(NULL, NULL, NULL, string[1]);
	}
	this->son->add(string + 1, offset);
      }
    } else {
      if (!this->brother) {
	this->brother = new Tree(NULL, NULL, NULL, string[0]);
      }
      this->brother->add(string, offset);
    }
  }
}

TYPEPTR Tree::get_adress() {
    return adress;
}

Tree *Tree::get_son() {
    return son;
}

void Tree::set_son(Tree *son) {
    this->son = son;
}

Tree *Tree::get_brother() {
    return brother;
}

void Tree::set_brother(Tree *brother) {
    this->brother = brother;
}

Info *Tree::get_info() {
    return info;
}

void Tree::set_info(Info *info) {
    this->info = info;
}

char Tree::get_car() {
    return car;
}

void Tree::get_car(char car) {
    this->car = car;
}
