//<head>//////////////////////////////////////////////////////////////
//
//	
//	Lexed version 4.0
//	Copyright (C) 1999 2000 2001 2002 2003.
//	Universit� Paris 7
//	INRIA Rocquencourt
//
//	Conception & programmation :
//	Lionel Cl�ment
//	e-mail : lionel.clement@linguist.jussieu.fr
//
//	vendredi 18 janvier 2002, 13h41
//
//
////////////////////////////////////////////////////////////////</head>////

#ifndef LIBLEXED_H
#define LIBLEXED_H

#include "lexedserver.h"
#include "lexedtree.h"

//#define BUG {fprintf (stderr, "*** Bug %s(%d)\n", __FILE__, __LINE__);}

#define LEXEDMAXSTRING 4096
#define LEXEDMAXREPLACE 64
#define LEXEDMAXRESULT 64 // pour les recherches approx pas de panique: au del� on r�aloue

int load_from_input(
  FILE *input,
  long input_size
);

int load_from_files(
  char *directory,
  char *prefix
);

int save_to_files(
  char *directory,
  char *prefix
);

int finish(); 

////////////////////////////////////////////////////////////
// input: info
// output: sending results to stdout/client
////////////////////////////////////////////////////////////
int print_results (
  TYPEPTR index,
 int client_server, 
 Server *server, 
 int sep
);

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
int sprint_results (
  TYPEPTR index,
  char ***result,
  int *result_size,
  int *result_index
);

////////////////////////////////////////////////////////////
// input:
// output: list_result pointer
////////////////////////////////////////////////////////////
list_result list(
);

////////////////////////////////////////////////////////////
// input:
// output: list_result pointer
////////////////////////////////////////////////////////////
int free_list_result(
  list_result *result
);

////////////////////////////////////////////////////////////
// input: chaine
// output: info
////////////////////////////////////////////////////////////
TYPEPTR search_static(
  TYPEPTR index,
  char *string
);

////////////////////////////////////////////////////////////
// input: string
// output: info
////////////////////////////////////////////////////////////
int search(
  TYPEPTR index,	// the index in the tree
  char *string,	// the string to find
  int nb_erase,	// number of chars to erase
  int nb_substitute, 	// number of chars to subst
  int nb_add, 	// number of chars to add
  int nb_swap, 	// number of chars to swap
  int nb_erase_double,// number of times a double is erased
  int nb_add_double, 	// number of times a double is add
  int nb_replace,	// number of chars to replace
  struct FindReplaceStructure *replace,	// couples (char *search, char *replace) -- (sets of chars)
  int already_modified,	// leng of the prefix of the rest already changed (to do not modify a char)
  TYPEPTR **result, 
  int *result_size,
  int *result_index,
  int weight
);

////////////////////////////////////////////////////////////
int
save_fsa(
  char *filename
);

int
save_table(
  char *filename
);

int
load_fsa(
  char *filename
);

int
load_table(
  char *filename
);

#endif
